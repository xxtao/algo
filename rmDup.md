# 26. Remove Duplicates from Sorted Array

## Problem
[ref](https://leetcode.com/problems/remove-duplicates-from-sorted-array/) 

Given a sorted array nums, remove the duplicates in-place such that each element appear only once and return the new length.

Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.

Example 1:  
>Given nums = [1,1,2],  
>Your function should return length = 2, with the first two elements of nums being 1 and 2 respectively.  
>It doesn't matter what you leave beyond the returned length.  

Example 2:  
>Given nums = [0,0,1,1,1,2,2,3,3,4],  
>Your function should return length = 5, with the first five elements of nums being modified to 0, 1, 2, 3, and 4 respectively.  
>It doesn't matter what values are set beyond the returned length.


## First native method
My first native idea is to remove every duplicated element. Removing an element `nums[i]` means to move every elements after the `nums[i]` one cell forward. 

```java
class Solution {
    public int removeDuplicates(int[] nums) {
        int result = nums.length;
        for(int i = nums.length - 1; i > 0 ; i--) {
            if(nums[i - 1] == nums[i]) {
                deleteElem(nums, i);
                result--;
            }
        }
        return result;
    }
    
    private void deleteElem(int[] array, int index) {
        for(int i = index; i < array.length - 1; i++) {
            array[i] = array[i+1];
        }
    }
}
```

Time complexity: O(N²)  
Space complexity: O(1)

## Two pointers method
Instead of removing every duplicate elements, we can only keep the non-dup elements, so that we just need to move the unique element until we have bypassed the duplicated ones.

In this method, we need two pointers: a slower one `i` for recoding the last unique element, another faster one `j` for recoding the traversing element.
Once the faster pointer finds the element `nums[j]` with different value than the slower pointer `nums[i]`, we can copy the element `nums[j]` to `nums[i+1]`.

```java
class Solution {
    public int removeDuplicates(int[] nums) {
        if (nums.length == 0) return 0;
        int i = 0;
        for(int j = 0; j < nums.length; j++) {
            if(nums[j] != nums[i]) {
                nums[i+1] = nums[j];
                i++;
            }
        }
        return i+1;
    }
}
```

Time Complexity: O(N)  
Space Complexity: O(1)  

During my implementation, I have forgotten two points: 
- We should return the array length `i+1`, instead of the last element index `i`.
- The edge case: empty array.