# 1. Two Sum (Array, HashTable)
## Problem
[ref](https://leetcode.com/problems/two-sum/#/description)

> Given an array of integers, return indices of the two numbers such that they add up to a specific target.  
> You may assume that each input would have exactly one solution, and you may not use the same element twice.  
> Example:
Given nums = [2, 7, 11, 15], target = 9,  
Because nums[0] + nums[1] = 2 + 7 = 9,  
return [0, 1].


## My Solutions
### First native solution (brute force)
#### Code
```java
class Solution {
    public int[] twoSum(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) return new int[]{i, j};
            }
        }
        return null;
    }
}
```
#### Complexity
Time complexity: O(N²)
Space complexity: O(1)

### Second Solution
We can also trade the space for time efficiency. 

For each specific element `nums[i]`, instead of traversing the array for matching the target, we can just search for the element complementary (i.e., `target - nums[i]`). 
And we can use `HashMap` to quickly get the complementary number.

Therefore, in this second solution, we first create a HashMap for storing the array elements (the element value as the key, and the element indice as the value). Then, we can search the complementary of each array element until got one.

Notice that, we need to verify the complementary found is not the element itself.

#### Code
```java
class Solution {
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> numsMap = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            numsMap.put(nums[i], i);
        }
        for (int i = 0; i < nums.length; i++) {
            Integer complementaryIndice = numsMap.get(target - nums[i]);
            if (complementaryIndice != null && complementaryIndice != i)
                return new int[]{i, complementaryIndice};
        }
        return null;
    }
}
```
#### Complexity
Time complexity: O(N)
Space complexity: O(N)


## Provided Solutions
[ref](https://leetcode.com/articles/two-sum/)

We can just do one pass instead of two (as shown in the previous solution).
That is, just check whether complementary existed before inserting each element into the map.
So we only search the complementary within the visited elements.
Be attention of the returned indices order.

#### Code
```java
class Solution {
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> numsMap = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            Integer complementaryIndice = numsMap.get(target - nums[i]);
            if (complementaryIndice != null)
                return new int[]{complementaryIndice, i};
            else
                numsMap.put(nums[i], i);
        }
        return null;
    }
}

```

#### Complexity
Time complexity: O(N)
Space complexity: O(N)