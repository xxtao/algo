# Algorithm Exercises

## LeetCode

| # | Problem | Solution |
|---| ------- | -------- |
|1|[Two Sum](https://leetcode.com/problems/two-sum/)|[Sol](./2sum.md)|